<?php

use yii\db\Migration;

/**
 * Class m190902_123654_create_table_user
 */
class m190902_123654_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `user` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `login` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `surname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
        `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
        `password_hash` text COLLATE utf8_unicode_ci,
        `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `status` smallint(6) NOT NULL DEFAULT '10',
        `created_at` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
        `gender` enum('man','woman','other') COLLATE utf8_unicode_ci DEFAULT NULL,
        `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `email` (`email`),
        UNIQUE KEY `login` (`login`),
        UNIQUE KEY `password_reset_token` (`password_reset_token`)
      ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        
        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $sql = "DROP TABLE `user`";
        
        \Yii::$app->db->createCommand($sql)->execute();
    }
}
