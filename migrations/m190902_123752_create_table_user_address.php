<?php

use yii\db\Migration;

/**
 * Class m190902_123752_create_table_user_address
 */
class m190902_123752_create_table_user_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `user_address` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `postcode` varchar(50) NOT NULL,
            `country` varchar(2) NOT NULL,
            `city` varchar(50) NOT NULL,
            `street` varchar(255) NOT NULL,
            `house_num` varchar(10) DEFAULT NULL,
            `office_num` varchar(10) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `user_id` (`user_id`),
            CONSTRAINT `user_address` 
            FOREIGN KEY (`user_id`) 
            REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION) 
            ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $sql = "DROP TABLE `user_address`";
        
        \Yii::$app->db->createCommand($sql)->execute();
    }
}
