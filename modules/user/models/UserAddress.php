<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\models\User;

/**
 * This is the model class for table "user_address".
 *
 * @property int $id
 * @property int $user_id
 * @property string $postcode
 * @property string $country
 * @property string $city
 * @property string $street
 * @property string $house_num
 * @property string $office_num
 *
 * @property User $user
 */
class UserAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['postcode', 'country', 'city', 'street'], 'required'],
            [['postcode', 'user_id'], 'integer'],
            [['city'], 'string', 'max' => 50],
            [['country'], 'string', 'max' => 2],
            [['street'], 'string', 'max' => 255],
            [['house_num', 'office_num'], 'string', 'max' => 10],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            
            [['country'], 'trim'],
            ['country', 'match', 'pattern' => '/(?=.*([A-Z].*?[A-Z]))/', 'message' => 'Некорректный код страны. Пример: UA' ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '',
            'user_id' => '',
            'postcode' => 'Почтовый индекс',
            'country' => 'Страна',
            'city' => 'Город',
            'street' => 'Улица',
            'house_num' => 'Номер дома',
            'office_num' => 'Номер офиса/квартиры',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function createUserAddress(User $model) {
        $this->user_id = $model->id;
        
        return ($this->save()) ? true : false;
    }

}
