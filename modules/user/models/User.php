<?php

namespace app\modules\user\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $username
 * @property string $surname
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property int $status
 * @property int $created_at
 * @property string $gender
 * @property string $phone
 *
 * @property UserAddress[] $userAddresses
 */
class User extends \yii\db\ActiveRecord {
    
    public $_gender = [];
    
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
//    public function behaviors() {
//        return [
//            TimestampBehavior::className(),
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['username', 'surname', 'email', 'login', 'gender'], 'required'],
            [['gender'], 'string'],
            [['status'], 'integer'],
            [['email', 'username', 'surname'], 'string', 'max' => 50],
            [['auth_key'], 'string', 'max' => 32],
            [['password_reset_token', 'phone'], 'string', 'max' => 255],

            ['login', 'trim'],
            ['login', 'string', 'min' => 4],
            
            
            ['password_hash', 'trim'],
            ['password_hash', 'required'],
            ['password_hash', 'string', 'min' => 6],
            
            ['phone', 'trim'],
            ['phone', 'unique'],
            ['phone', 'match', 'pattern' => '/^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/', 'message' => 'Некорректный номер телефона' ],
            
            [['email'], 'unique'],
            [['email'], 'email'],
            [['login'], 'unique'],
            [['password_reset_token'], 'unique'],
            
            [['username', 'surname'], 'match', 'pattern' => '/^[A-ZА-ЯЁІ]/', 'message' => 'Первая буква должна быть большой'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'email' => 'Email',
            'username' => 'Имя',
            'surname' => 'Фамилия',
            'auth_key' => '',
            'password_hash' => 'Пароль',
            'password_reset_token' => '',
            'status' => 'Status',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'gender' => 'Стать',
            'phone' => 'Телефон',
        ];
    }
    
    public function validateFirstLetter($attribute, $params)
    {
        if (!ctype_upper($this->$attribute{0})) {
            $this->addError($attribute, 'Первая буква должна быть большой');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAddresses() {
        return $this->hasMany(UserAddress::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $userEmail
     * @return static|null
     */
    public static function findByUserEmail($userEmail) {
        return static::findOne(['email' => $userEmail, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    
    public function signup() {
        $this->created_at = date('c');
        $this->gender = $this->_gender[$this->gender];
        $this->setPassword($this->password_hash);
        $this->generateAuthKey();

        return ($this->save()) ? true : false;
    }

}
