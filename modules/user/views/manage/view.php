<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
            'email:email',
            'username',
            'surname',
            'status',
            [
                'attribute' => 'created_at',
                'value' => function ($model) {

                    return Yii::$app->formatter->asDateTime($model->created_at, 'php:d-m-Y H:m');
                },
            ],
            'phone',
            'gender',
        ],
    ])
    ?>

    <div class="cabinet__admin-table-wrap">
        <?=
        GridView::widget([
            'dataProvider' => $dPAddress,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'postcode',
                    'label' => 'Почтовий индекс',
                ],
                [
                    'attribute' => 'country',
                    'label' => 'Страна',
                ],
                [
                    'attribute' => 'city',
                    'label' => 'Город',
                ],
                [
                    'attribute' => 'street',
                    'label' => 'Улица',
                ],
                [
                    'attribute' => 'house_num',
                    'label' => 'Номер дома',
                ],
                [
                    'attribute' => 'office_num',
                    'label' => 'Номер офиса/квартиры',
                ],
                [
                    'class' => ActionColumn::class,
                    'header' => 'Управление',
                    'controller' => '/user/address',
                    'template' => '{update} {delete}',
                ],
            ],
        ]);
        ?>


    </div>


    <div class="cabinet__create-user">
        <!-- <div class="modal__window-close">&#10006;</div> -->
        <?php
        $form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'method' => 'post',
            'action' => Url::to(['/user/address/create']),
            'validationUrl' => Url::to(['/user/address/create-validate']),
        ]);
        ?>
        <h2 for="name3" class="cabinet__title">Добавить адрес</h2>
        <div class="row">
            <?= $form->field($userAddress, 'user_id')->hiddenInput() ?>
            <?= $form->field($userAddress, 'postcode')->input('text') ?>
            <?= $form->field($userAddress, 'country')->input('text') ?>
            <?= $form->field($userAddress, 'city')->input('text') ?>
            <?= $form->field($userAddress, 'street')->input('text') ?>
            <?= $form->field($userAddress, 'house_num')->input('text') ?>
            <?= $form->field($userAddress, 'office_num')->input('text') ?>
        </div>
        <input type="submit" class="cabinet__create-user-btn btn btn_bg-blue" value="Добавить" style="float: right">
        <?php ActiveForm::end(); ?>
    </div>