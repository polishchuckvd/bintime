<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>

<div class="cabinet__create-user">
    <!-- <div class="modal__window-close">&#10006;</div> -->
    <?php
    $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'method' => 'post',
//        'action' => Url::to(['/cart/cart/checkouts']),
        'validationUrl' => Url::to(['/user/manage/create-validate']),    
    ]);
    ?>
        <h2 for="name3" class="cabinet__title">Регистрация</h2>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($modelUser, 'login')->input('text') ?>
                <?= $form->field($modelUser, 'password_hash')->passwordInput() ?>
                <?= $form->field($modelUser, 'email')->input('text') ?>
                <?= $form->field($modelUser, 'username')->input('text') ?>
                <?= $form->field($modelUser, 'surname')->input('text') ?>
                <?= $form->field($modelUser, 'phone')->input('text') ?>
                <?= $form->field($modelUser, 'gender')->dropDownList($modelUser->_gender) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($modelAddress, 'postcode')->input('text') ?>
                <?= $form->field($modelAddress, 'country')->input('text') ?>
                <?= $form->field($modelAddress, 'city')->input('text') ?>
                <?= $form->field($modelAddress, 'street')->input('text') ?>
                <?= $form->field($modelAddress, 'house_num')->input('text') ?>
                <?= $form->field($modelAddress, 'office_num')->input('text') ?>
                
            </div>
        </div>
        <input type="submit" class="cabinet__create-user-btn btn btn_bg-blue" value="Сохранить" style="float: right">
    <?php ActiveForm::end(); ?>
</div>