<?php

namespace app\modules\user\components\repositories;

use app\modules\user\models\UserAddress;
use Yii;
/**
 * Description of UserPhoneRepository
 *
 * Class ShopCategoryRepository
 * @package app\modules\user\components\repositories
 */
class AddressRepository {

    /**
     * @param $id
     * @return int|null
     */
    public function getCountUserAddress($id) {
        return UserAddress::find()->where(['user_id' => $id])->count();
    }

}
