<?php

namespace app\modules\user\components\repositories;

use app\modules\user\models\User;
use Yii;
/**
 * Description of UserPhoneRepository
 *
 * Class ShopCategoryRepository
 * @package app\modules\user\components\repositories
 */
class UserRepository {

    /**
     * Get options by gender property
     * @return array|null
     */
    public function getGenderVariants() {
        $sql = "SHOW COLUMNS FROM `user` WHERE Field = 'gender'";
        
        $type = \Yii::$app->db->createCommand($sql)->queryOne();
        
        preg_match("/^enum\(\'(.*)\'\)$/", $type['Type'], $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

}
