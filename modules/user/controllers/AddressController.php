<?php

namespace app\modules\user\controllers;

use Yii;
use app\modules\user\models\User;
use app\modules\user\models\Manage;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use app\modules\user\models\UserAddress;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use app\modules\user\components\repositories\AddressRepository;

/**
 * ManageController implements the CRUD actions for User model.
 */
class AddressController extends Controller {

    private $addressRepository;


    public function __construct($id, $module, 
            AddressRepository $addressRepository,  $config = []) {

        $this->addressRepository = $addressRepository;

        parent::__construct($id, $module, $config);
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Verify UserAddress creation form
     * @return mixed
     */
    public function actionCreateValidate() {
        if (\Yii::$app->request->isAjax) {
            $modelAddress = new UserAddress();

            if ($modelAddress->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($modelAddress);
            }
        }

        throw new HttpException(500);
    }

    /**
     * Creates a new UserAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        $modelAddress = new UserAddress();

        if ($modelAddress->load(Yii::$app->request->post()) && $modelAddress->validate()) {

            if ($modelAddress->save()) {
                return $this->redirect(['/user/manage/view', 'id' => $modelAddress->user_id]);
            }
            throw new NotFoundHttpException('Не удалось сохранить пользователя!');
        }

        return $this->render('create', [
                    'modelUser' => $modelUser,
                    'modelAddress' => $modelAddress,
        ]);
    }

    /**
     * Updates an existing UserAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/user/manage/view', 'id' => $model->user->id]);
        }

        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        
        $user_id = $model->user_id;
        
        if($this->addressRepository->getCountUserAddress($user_id) > 1) {
            $model->delete();
        } 

        return $this->redirect(['/user/manage/view', 'id' => $user_id]);
    }

    /**
     * Finds the UserAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = UserAddress::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
