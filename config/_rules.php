<?php

use yii\web\GroupUrlRule;

return [
    '/' => 'user/manage',
    
    
    '<controller:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
];
